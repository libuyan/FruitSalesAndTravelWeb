import request from 'utils/request.js'

const api = {}

//用户登录api接口
api.login = (data) => request.post('/user/login', data)

//用户登录api接口
api.getUserInfo = (token) => request.get('/user/info?token=' + token)

//用户登录api接口
api.updateUser = (data) => request.put('/user', data)

//delete请求示例
api.admUpdataRole = (id) => request.delete('/user?id=', id)

//put请求示例
api.admUpdataInfo = (user) => request.put('/user', user)

//get请求示例
api.verify = (phone) => request.get('/user/isExits?tel=' + phone)


//购物车
api.cart = (userId) => request.get("/cart?userId")
//修改景区信息
api.updatascenic = (scenic) => request.put('/scenic', scenic)


// 添加景点
api.addScenic = (data) => request.post('/scenic', data)

// 删除景点
api.delScenic = (scenicId) => request.delete('/scenic?scenicId=', scenicId)

// 添加景点门票
api.addTicket = (data) => request.post('/ticket', data)

//获取门票列表
api.getTicketList = (scenicId) => request.get('/ticket?scenicId=', scenicId)

// 删除门票
api.delTicket = (ticketId) => request.delete('/ticket?ticketId=', ticketId)

//添加水果
api.addfruits = (data) => request.post('/fruit', data)

//修改水果信息
api.updatafruits = (fruit) => request.put('​/fruit​/updateFruit', fruit)

//修改水果子项信息
api.updataChildFruit = (childFruit) => request.put('​/childFruit', childFruit)

//单个水果数据
api.getFruits = (fruitId) => request.get('//fruit?fruitId=' + fruitId)

// 添加水果子项
api.addchildFruit = (data) => request.post('/childFruit', data)

// 查找单个水果子项信息
api.getchildFruit = (ChildFruitId) => request.get('/childFruit/getOneChildFruit?ChildFruitId=' + ChildFruitId)

//获取水果子项列表
api.getChildFruitList = (fruitId) => request.get('/childFruit/getChildFruitList?fruitId=' + fruitId)

//删除水果
api.delFruit = (fruitId) => request.delete('/fruit?fruitId=' + fruitId)

// 获取管理员列表
api.getManagerList = (token) => request.get('/user/getAllAdmin?token=' + token)

//创建订单
api.submitOrder = (orderVO) => request.post('/OrderForm/createFakeOrder?', orderVO)
//查找所有水果
api.getFruitList = (page, pageSize) => request.get('/fruit/getFruits?page=' + page + '&pageSize=' + pageSize)
//查找所有景区
api.getScenicList = (page, pageSize) => request.get('/scenic/getAllScenic?page=' + page + '&pageSize=' + pageSize)
//获取水果推荐列表
api.getRecommendFruitList = (token) => request.get('/rec/fruitRec')
//获取景区推荐列表
api.getRecommendScenicList = (token) => request.get('/rec/scenicRec')
//删除水果子项
api.delChildFruit = (childFruitId) => request.delete('/fruit?childFruitId=' + childFruitId)

// 获取管理员列表
api.getManagerList = (token) => request.get('/user/getAllAdmin?token=' + token)

// 获取图片
api.getImageLiat = (imgType, productId) => request.get('/img/getImg?imgType=' + imgType + "&productId=" + productId)

// 绑定图片
api.bindImage = (imgId, type, productId) => request.post('/img/bind?imgId=' + imgId + "&type=" + type + "&productId=" +
	productId)

// 管理员获取全部状态的订单
api.getAllOrder = (token) => request.post('/OrderForm/getAllOrdersByAdmin?token=' + token)

// /OrderForm/getOrdersByAdmin
api.getStatusOrder = (orderStatus) => request.post('/OrderForm/getOrdersByAdmin?orderStatus=' + orderStatus)

//获取所有订单
api.getOrder = () => request.post('/OrderForm/getAllOrder')

api.verify = (phone) => request.get('/user/isExits?tel=' + phone)
//景区部分数据
api.getScenic = (scenicId) => request.get('/scenic?scenicId=' + scenicId)

// //查找所有水果
// api.getFruitList = (page, pageSize) => request.get('/fruit/getFruits?page=' + page + '&pageSize=' + pageSize)

// //查找所有景区
// api.getScenicList = (page, pageSize) => request.get('/scenic/getAllScenic?page=' + page + '&pageSize=' + pageSize)

//景区详情数据
api.getScenicDetail = (scenicId) => request.get('/scenic?scenicId=' + scenicId)

//景区门票详情数据
api.getSticketsList = (scenicId) => request.get('/ticket?scenicId=' + scenicId)
//水果详情数据
api.getFruitChildDetail = (fruitId) => request.get('/fruit/getFruitAndChildFruits?fruitId=' + fruitId)
//获取水果评价
api.getFruitEvaluateList = (productId) => request.get('/evaluate/fruitEvaluateList?productId=' + productId)
//获取所有水果一周/月/年的销量的报表数据 参数：ago，1 -周  2 - 月  3 - 年
api.getFruitSaleVolumeByAgo = (ago) => request.get('/getAllFruitSalesData?ago=' + ago)

//获取所有水果一周/月/年的评价的报表数据 参数：ago，1 -周  2 - 月  3 - 年  evaluationType，1 - 好评  2 - 一般  3 - 差评
api.getFruitSaleVolumeByAgo = (ago, evaluationType) => request.get('//getAllFruitEvalData?ago=' + ago +
	'&evaluationType=' + evaluationType)

//获取订单以及商品信息
api.getOrderForm = (orderFormId) => request.post('/OrderForm/getAllOrder?orderFormId=' + orderFormId)

//创建订单
api.createOrder = (data) => request.post('/OrderForm/createFakeOrder', data)

//获取支付信息
api.pay = (orderId) => request.post('/OrderForm/fakePay?', orderId)

//获取购物车列表
api.getShopCart = () => request.get('/cart')

//添加购物车水果项
api.addCartFruits = (data) => request.post('/cart', data)

//获取全部地址
api.getAllAddress = () => request.post('/delivery/getAll')

//获取默认地址
api.getDefaultAddress = () => request.post('/delivery/getDefault')

//添加地址
api.addAddress = (deliveryInfo) => request.put('/delivery', deliveryInfo)

//删除收获地址
api.deleteAddress = (deliId) => request.delete(`/delivery?deliId=`, deliId)

//搜索
api.searchFruit = (nameLike) => request.get('/fruits/fruit_list?nameLike=' + nameLike)
api.searchScenic = (nameLike) => request.get('/scenic/scenicList?nameLike=' + nameLike)
api.getSearchList = (nameLike) => request.get('/search?nameLike=' + nameLike)

//获取图片
api.getUrlList = (imgType, productId) => request.get('/img/getImg?imgType=' + imgType + '&productId=' + productId)
api.getFruitDetail = (fruitId) => request.get('/fruits?fruitId=' + fruitId)
//获取所有水果一周/月/年的销量的报表数据 参数：ago，1 -周  2 - 月  3 - 年
api.getFruitSaleVolumeByAgo = (ago) => request.get('/getAllFruitSalesData?ago=' + ago)
//获取所有水果一周/月/年的评价的报表数据 参数：ago，1 -周  2 - 月  3 - 年  evaluationType，1 - 好评  2 - 一般  3 - 差评
api.getFruitSaleVolumeByAgo = (ago, evaluationType) => request.get('//getAllFruitEvalData?ago=' + ago +
	'&evaluationType=' + evaluationType)
//获取订单以及商品信息
api.getOrderForm = (orderFormId) => request.post('/OrderForm/getOrderAndProduct?orderFormId=' + orderFormId)
//获取支付信息
api.pay = (orderId) => request.post('/OrderForm/fakePay?orderId=' + orderId)
//获取购物车列表
api.getShopCart = () => request.get('/cart')
//获取全部地址
api.getAllAddress = () => request.post('/delivery/getAll')
//获取默认地址
api.getDefaultAddress = () => request.post('/delivery/getDefault')
//添加地址
api.addAddress = (deliveryInfo) => request.put('/delivery', deliveryInfo)
//删除收获地址
api.deleteAddress = (deliId) => request.delete(`/delivery?deliId=`, deliId)

//搜索
api.getFruitChildDetail = (fruitId) => request.get('/fruit/getFruitAndChildFruits?fruitId=' + fruitId)

//水果子项详情数据
api.getFchildList = (fruitId) => request.get('/childFruit/getChildFruitList?fruitId=' + fruitId)

//水果评价详情数据
api.getfEvaluate = (evaluateId) => request.get('/evaluate/searchFruitEvaluate?evaluateId=' + evaluateId)

//景区评价详情数据
api.getsEvaluate = (evaluateId) => request.get('/evaluate/searchScenicEvaluate?evaluateId=' + evaluateId)

//获取水果评价
api.getFruitEvaluateList = (productId) => request.get('/evaluate/fruitEvaluateList?productId=' + productId)

//获取景区评价
api.getScenicEvaluateList = (productId) => request.get('/evaluate/scenicEvaluateList?productId=' + productId)

//获取追评
api.getReevaluatelist = (evaluateId) => request.get('/evaluate/searchReevaluate?evaluateId=' + evaluateId)

export default api
