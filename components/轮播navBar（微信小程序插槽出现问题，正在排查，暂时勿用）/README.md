# yhao-swiperNavBar

#### 介绍
导航栏组件,导航自定义长度，页面数据自定义，左右切换，高度自由，上拉加载，下拉刷新

#### 软件架构
* 自定义navBar,实际项目需求
* 列表可左右切换
* 微信小程序		H5		App
* 增加下拉刷新和触底加载
* 加载更多动画	uni-load-more



#### 安装教程

1.  [组件下载](https://ext.dcloud.net.cn/plugin?id=5370)


#### 使用说明


## 基本用法
在 template 中的使用
```
<view class="">
	<!--组件-->
	<swiperNavBar ref="navbarSwiper" :scrollIntoView="scrollIntoView" :swiperTabList='swiperTabList' :swiperTabIdx='swiperTabIdx'
		:currentList="currentList" :curNavbar="curNavbar" v-if="swiperTabList.length > 1"  :showLoadMore="showLoadMore" @change="CurrentTab"
		@swiperChange="SwiperChange" @refresherrefresh="handleRefresherrefresh" @scrolltolower="handleScrolltolower">
		<!-- 自己需要的订单样式 S -->
		<template v-slot:slotItem="{ item, curNavbar }">
			<view class="list-item">
				{{item}}
			</view>
		</template>
		<!-- 自己需要的订单样式 E -->
	</swiperNavBar>
	<!--组件-->
</view>
```
在 script 中的使用
```
	import swiperNavBar from "@/components/swiperNavBar/swiperNavBar.vue";
	export default {
		data() {
			return {
				 /* 轮播导航 S */
				scrollIntoView: 0, //设置哪个方向可滚动，则在哪个方向滚动到该元素 轮播切换
				swiperTabList: [	//导航列表
					{ name: '导航1', alias: 0 }, 
					{ name: '导航2', alias: 1 }, 
					{ name: '导航3', alias: 2 }, 
					{ name: '导航4', alias: 3 }, 
					{ name: '导航5', alias: 4 }, 
					{ name: '导航6', alias: 5 }, 
					{ name: '导航7', alias: 6 }
					],
				swiperTabIdx: 0, // 轮播 切换
				curNavbar: {}, // 选中的导航数据  alias可以用来想后端请求的type
				loadMoreText: "加载中...",
				showLoadMore: 'more', // 加载更多动画

				currentList: [], // 当前需要展示的数据 是个数组
				 /* 轮播导航 E */
				pageNum: 1, // 分页的页码
				last_page: 1, // 总页码
			}
		},
		onLoad() {
			this.curNavbar = this.swiperTabList[this.swiperTabIdx] // 初始化选中项的数据参数
			this.getDataList()
		},
		components: {
			swiperNavBar
		},

		watch: {
			swiperTabIdx() {
				this.getDataList()
			}
		},
		methods: {
			// 请求后台接口，获取数据
			getDataList() {
				uni.showLoading({
					title: '加载中...',
					mask: true
				})
				setTimeout(() => {
					let arr = []
					for (let i = this.swiperTabIdx + (this.pageNum * 10); i >= 0; i--) {
						arr.push(i)
					}
					this.currentList = arr
					this.last_page = this.swiperTabIdx
					uni.hideLoading()
					if(this.$refs.navbarSwiper) {
						this.$refs.navbarSwiper.stopPullDownRefresh()
					}
					console.log('获取数据');
				}, 500)
				// console.log(this.currentList);
			},
			//navBar点击事件 
			CurrentTab: function(index, item) {
				this.swiperTabIdx = index;
				this.scrollIntoView = Math.max(0, index - 1);

			},
			//swiper滑动事件  
			SwiperChange: function(e) {
				this.swiperTabIdx = e.swiperTabIdx;
				this.scrollIntoView = e.scrollIntoView
			},
			// 下拉刷新
			handleRefresherrefresh() {
				// console.log('下拉刷新');
				this.pageNum = 1
				this.getDataList()
			},
			// 滚动到底部
			handleScrolltolower() {
				console.log('触底加载');
				this.showLoadMore = "loading"
				if (this.pageNum < this.last_page) {
					this.pageNum++
					this.getDataList();
				} else {
					this.showLoadMore = "noMore!"
				}
			}
		}

	}
```


## API
|  参数   		  	 | 必传  |   类型   | 默认值 |  说明 | 
|  ---- 			 | ----  |  ----   | ----   | ---- |
| scrollIntoView 	 | 是 	 |  Number | 0		| 设置哪个方向可滚动，则在哪个方向滚动到该元素	|
| swiperTabIdx 		 | 是 	 |  Array  | []		| 导航列表	|
| swiperTabList 	 | 是 	 |  Number | 0		| 导航对应列表下标	|
| currentList	 	 | 是 	 |  Array  | []		| 当前展示的列表	|
| curNavbar		 	 | 是 	 |  Object |  {}	| 当前选中的导航的值	|
| showLoadMore	 	 | 否 	 |  String |  more	| 加载更多的显示状态 按需必传	|
| navBarBackground 	 | 否 	 |  String | #ffffff| 导航的背景色	|
| swiperCurrentSize	 | 否 	 |  String | 26rpx	| 导航的字体大小	|
| swiperColor	 	 | 否 	 |  String | #333333| 导航栏字体未选中前颜色	|
| swiperCurrentColor | 否 	 |  String | #1D63FF| 选中当前导航栏字体颜色	|
| navBarWidth		 | 否 	 |  String | 20%	| 导航的宽度	|
| currentSwiperHeight| 否 	 |  Number | 88		| 当前导航的高度度	|
| currentSwiperLineShow| 否  |  Boolean| true	| 是否显示线条	|
| currentSwiperLineActiveWidth| 否 	 |  String | 60rpx| 当前选中的导航栏线条的宽度	|
| currentSwiperLineActiveHeight| 否  |  String | 6rpx| 当前选中的导航栏线条的高度度	|
| currentSwiperLineActiveBg| 否 	 |  String | #1D63FF| 当前选中的导航栏线条颜色	|
| currentSwiperLineAnimatie| 否 	 |  Boolean | true| 当前选中的导航栏线条过渡效果	|
| swiperMarginTop 	 | 否 	 |  String | 0rpx| 内容区距离navbar的margin-top	|
| headBorderColor	 | 否 	 |  String | 无		| 导航的底边线 |
| swiperBoxHeight	 | 否 	 |  String | calc(100vh - 88rpx - 88rpx)	| swiper可滚动高度	|



